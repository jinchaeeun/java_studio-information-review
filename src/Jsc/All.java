package Jsc;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Font;
import java.awt.Graphics;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JPanel;

import Jsc.MainPage.MyPanel;

public class All extends JFrame{
	JFrame frame;
	Container contentPane;
	ImageIcon dokyeong, duri, hosi, gyu, jsc, dus, co;
	Font c, kyeong, dolor;
	public All(String str){
		super();
		
		init();
		all();
		
	}
	
	public void init(){
	}
	

	public void all(){
		frame = new JFrame("전체 원룸 소개");
		Container contentPane = frame.getContentPane();
		dokyeong = new ImageIcon("도경 원룸.jpg");
		duri = new ImageIcon("두리 원룸.jpg");
		hosi = new ImageIcon("호시 원룸.jpg");
		gyu = new ImageIcon("규태 원룸.png");
		jsc = new ImageIcon("진성 원룸.png");
		dus = new ImageIcon("연아 원룸.jpg");
		co = new ImageIcon("채은 원룸.jpg");
		contentPane = getContentPane();
		
		MyPanel panel = new MyPanel();
		
		
		contentPane.add(panel, BorderLayout.CENTER);
		setSize(1100, 800);
		setVisible(true);

		c = new Font("고딕", Font.BOLD, 18);
		kyeong = new Font("고딕", Font.PLAIN, 13);
		dolor = new Font("고딕", Font.BOLD, 13);
		
		
	}
	class MyPanel extends JPanel {
		public void paintComponent(Graphics g) {
			super.paintComponent(g);
			
			g.setFont(c);
			
			g.drawString("전체 원룸 소개", 480, 42);
			g.drawLine(35,58,1050,58);
			
			g.drawImage(gyu.getImage(), 50, 80, 230, 130, this);
			g.setFont(kyeong);
			g.drawString("[JSC]규태 원룸", 60, 230);
			g.setFont(dolor);
			g.drawString("250만원", 60, 260);
			g.setFont(kyeong);
			g.drawString("경북 안동시 논골길 38", 60, 280);
			g.drawString("주인:김규태", 60, 300);
			g.drawString("옵션:옷장,tv,인덕션,인터넷,에어컨,", 60, 320);
			g.drawString("침대,책상,냉장고,전자렌지,정수", 60, 340);
			g.drawString("(각층복도에설치),전자도어락,세탁기", 60, 360);
			
			
			g.drawImage(dokyeong.getImage(), 300, 80, 230, 130, this);
			g.setFont(kyeong);
			g.drawString("[JSC]도경 원룸", 310, 230);
			g.setFont(dolor);
			g.drawString("220만원", 310, 260);
			g.setFont(kyeong);
			g.drawString("경북 경북 안동시 논골길 20", 310, 280);
			g.drawString("주인:김도경", 310, 300);
			g.drawString("옵션:옷장,인덕션,인터넷,에어컨,침대,", 310, 320);
			g.drawString("책상,냉장고,정수기(각층복도에설치),", 310, 340);
			g.drawString("전자도어락,세탁기", 310, 360);
			
			g.drawImage(duri.getImage(), 550, 80, 230, 130, this);
			g.setFont(kyeong);
			g.drawString("[JSC]두리 원룸", 560, 230);
			g.setFont(dolor);
			g.drawString("230만원", 560, 260);
			g.setFont(kyeong);
			g.drawString("경북 안동시 송천1길 42", 560, 280);
			g.drawString("주인:김두리", 560, 300);
			g.drawString("옵션:옷장,tv,가스렌지,인터넷,에어컨,", 560, 320);
			g.drawString("책상,냉장고,전자렌지,정수기(각층복", 560, 340);
			g.drawString("도에설치),세탁기", 560, 360);
			
			
			//연아 원룸 사진 추가
			g.drawImage(dus.getImage(), 800, 80, 230, 130, this);
			g.setFont(kyeong);
			g.drawString("[JSC]연아 원룸", 800, 230);
			g.setFont(dolor);
			g.drawString("230만원", 800, 260);
			g.setFont(kyeong);
			g.drawString("경북 안동시 논골길 12", 800, 280);
			g.drawString("주인:최연아", 800, 300);
			g.drawString("옵션:옷장,tv,가스렌지,인터넷,에어컨,", 800, 320);
			g.drawString("책상,냉장고,전자렌지,정수기(각층복", 800, 340);
			g.drawString("도에설치),세탁기", 800, 360);
			
			g.drawImage(jsc.getImage(), 50, 400, 230, 130, this);
			g.setFont(kyeong);
			g.drawString("[JSC]진성 원룸", 50, 550);
			g.setFont(dolor);
			g.drawString("(1년)210만원", 50, 580);
			g.setFont(kyeong);
			g.drawString("경북 안동시 송천2길 15 ", 50, 600);
			g.drawString("주인:황진성", 50, 620);
			g.drawString("옵션:옷장,가스렌지,인터넷,에어컨,책", 50, 640);
			g.drawString("상,냉장고,전자렌지,정수기(각층복도", 50, 660);
			g.drawString("에설치),전자도어락,세탁기", 50, 680);
			
			
			g.drawImage(co.getImage(), 300, 400, 230, 130, this);
			g.setFont(kyeong);
			g.drawString("[JSC]채은 원룸", 310, 550);
			g.setFont(dolor);
			g.drawString("200만원", 310, 580);
			g.setFont(kyeong);
			g.drawString("경북 안동시 송천1길 17", 310, 600);
			g.drawString("주인:진채은", 310, 620);
			g.drawString("옵션:옷장,가스렌지,인터넷,에어컨,책", 310, 640);
			g.drawString("상,침대,냉장고,전자렌지,정수기(각층", 310, 660);
			g.drawString("복도에설치),전자도어락,심야전기난방", 310, 680);
			
			
			
			g.drawImage(hosi.getImage(), 550, 400, 230, 130, this);
			g.setFont(kyeong);
			g.drawString("[JSC]호시 원룸", 560, 550);
			g.setFont(dolor);
			g.drawString("180만원", 560, 580);
			g.setFont(kyeong);
			g.drawString("경북 안동시 송천1길 17-1", 560, 600);
			g.drawString("주인:권순영", 560, 620);
			g.drawString("옵션:옷장,tv,인덕션,인터넷,에어컨,", 560, 640);
			g.drawString("침대,책상,냉장고,전자렌지,정수기", 560, 660);
			g.drawString("(각층복도에설치),세탁기", 560, 680);
			
			g.drawLine(35,710,1050,710);

			setBackground(Color.WHITE);
		}
	}
	
}