package Jsc;


import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import javax.swing.JComboBox;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;


public class EventActionListener implements ActionListener {
    JTable table;
    JTextArea text2;
    JTextField text1;
	JComboBox<String> text3;
	JComboBox<String> text4;
    
    EventActionListener() {
    }
    EventActionListener(JTable table) {
  this.table = table;
 }
    EventActionListener(JTable table, JTextField text1, 
                     	JTextArea text2, JComboBox<String> text3, JComboBox<String> text4) {
        this.table = table;
        this.text1 = text1;
        this.text2 = text2;
        this.text3 = text3;
        this.text4 = text4;
    }
   
	public void actionPerformed(ActionEvent e) {
     if (e.getActionCommand().equals("조회"))//getActionCommand 버튼이 눌림
     {
      select();
     }
     else if (e.getActionCommand().equals("추가"))
     {
      insert();
     }
     else  if (e.getActionCommand().equals("삭제"))
     {
      delete();
     }
     else
     {
      update();
     }
    }
    
    void select()
    {
        String driver = "oracle.jdbc.driver.OracleDriver";
        String url = "jdbc:oracle:thin:@127.0.0.1:1521:xe";
        Connection con = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        
        String arr[] = new String[4];
        
        DefaultTableModel model = (DefaultTableModel) table.getModel();
        int rowNum = model.getRowCount();
        for (int i=0; i<rowNum; i++)
         model.removeRow(0);
        
        try {
         String sql = "select * from review1 order by irum";
         
            Class.forName(driver);
            con = DriverManager.getConnection(url, "scott", "tiger");

            pstmt = con.prepareStatement(sql);
            rs = pstmt.executeQuery(sql);

            while(rs.next()) {
                arr[0] = rs.getString("irum");
                arr[1] = /*Integer.toString(rs.getInt("age"));*/rs.getString("age");
                arr[2] = rs.getString("phone");
                arr[3] = rs.getString("he");
                model.addRow(arr);
            }
        }
        catch(Exception e) {
            System.out.println(e.getMessage());
            e.printStackTrace( );
        }
        finally {
         try {rs.close();} catch (Exception ignored) {}
         try {pstmt.close();} catch (Exception ignored) {}
         try {con.close();}catch (Exception ignored) {}
        }
    }
    
    void insert()
    {
        String arr[] = new String[4];
        arr[0] = text1.getText().trim();
        //System.out.println("00"+arr[0]+"00");
        
//        if(arr[0].equals("")) {
//        	return;
//        }
        arr[1] = text2.getText().trim();
        arr[2] = text3.getSelectedItem().toString().trim();
        arr[3] = text4.getSelectedItem().toString().trim();
        
        DefaultTableModel model = (DefaultTableModel) table.getModel();
        
       

        
        String driver = "oracle.jdbc.driver.OracleDriver";
        String url = "jdbc:oracle:thin:@127.0.0.1:1521:xe";
        Connection con = null;
        PreparedStatement pstmt = null;

        String sql = null;
        int rowNum;

        try {
         sql = "insert into review1 (irum, age, he, phone) values (?, ?, ?, ?)";  //원본
        // sql = "insert into review1 (irum) values (?)";
            Class.forName(driver);
            con = DriverManager.getConnection(url, "scott", "tiger");
            pstmt = con.prepareStatement(sql);
            pstmt.setString(1, arr[0]); 
            /*pstmt.setInt(2, Integer.parseInt(arr[1])); */ //원본
            pstmt.setString(2, arr[1]); 
            pstmt.setString(3, arr[2]); 
            pstmt.setString(4, arr[3]); 
            
            rowNum = pstmt.executeUpdate();  
            if (rowNum == 0)
             System.out.println("데이터 추가 실패!");
            else
            {
             model.addRow(arr);
             System.out.println("데이터 추가 성공!");
            }
        }
        catch(Exception e1) {
            System.out.println("데이터 추가 실패!! = "+e1.getMessage());
        }
        finally {
         try {pstmt.close();} catch (Exception ignored) {}
            try {con.close();}catch (Exception ignored) {}
        }
    }
    
    void delete()
    {
     int row = table.getSelectedRow();
        if (row == -1)
            return;
        
        DefaultTableModel model = (DefaultTableModel) table.getModel();
        String irum = model.getValueAt(row, 0).toString().trim();
       
        
        String driver = "oracle.jdbc.driver.OracleDriver";
        String url = "jdbc:oracle:thin:@127.0.0.1:1521:xe";
        Connection con = null;
        PreparedStatement pstmt = null;

        String sql = null;
        int rowNum;
        
        try {  
            sql = "delete from review1 where irum = ?";

            Class.forName(driver);
            con = DriverManager.getConnection(url, "scott", "tiger");
            pstmt = con.prepareStatement(sql);
            pstmt.setString(1, irum);
            rowNum = pstmt.executeUpdate();
            if (rowNum == 0)
             System.out.println("데이터베이스 삭제 실패!");
            else
            {
             model.removeRow(row); 
             System.out.println("데이터베이스 삭제 성공!");
            }
        }
        catch(Exception e1) {
            System.out.println("데이터베이스 삭제 실패!"+e1.getMessage());
        }
        finally {
         try {pstmt.close();} catch (Exception ignored) {}
            try {con.close();}catch (Exception ignored) {}
        }
    }
    
    void update()
    {
     int i, /*age,*/ rowNum;
     /*String irum = null, phone = null, table_irum = null;*/ //원본
     String irum = null, phone = null, age = null ,he = null ,table_irum = null;
        DefaultTableModel model = (DefaultTableModel) table.getModel();
        
        irum = text1.getText().toString().trim();
      //  age = Integer.parseInt(text2.getText().toString().trim()); //원본
        age = text2.getText().toString().trim();
        phone = text3.getSelectedItem().toString().trim();
        he = text4.getSelectedItem().toString().trim();
        
        String driver = "oracle.jdbc.driver.OracleDriver";
        String url = "jdbc:oracle:thin:@127.0.0.1:1521:xe";
        Connection con = null;
        PreparedStatement pstmt = null;

        String sql = null;
        
        try {  
            sql = "update review1 set age = ?, phone = ? irum = ? where he = ?";//원본
           // sql = "update review1 where irum = ?";
            System.out.println("sql : " + sql);
            Class.forName(driver);
            con = DriverManager.getConnection(url, "scott", "tiger");
            pstmt = con.prepareStatement(sql);
           /* pstmt.setInt(1, age);*/ //원본
            pstmt.setString(1, age);
            pstmt.setString(2, phone);
            pstmt.setString(3, irum); 
            pstmt.setString(4, he); 
            rowNum = pstmt.executeUpdate();
            
            if (rowNum == 0)
             System.out.println("데이터베이스 수정 실패!");
            else
            {
             rowNum = model.getRowCount();
                for (i=0; i<rowNum; i++)
                {
                 table_irum = model.getValueAt(i, 0).toString().trim();
                 if (table_irum.equals(irum))
                 {
                  model.setValueAt(irum, i, 0);
                  model.setValueAt(age, i, 1);
                  model.setValueAt(phone, i, 2);
                  model.setValueAt(he, i, 3);
                  break;
                 }
                }
             System.out.println("데이터베이스 수정 성공!=" + rowNum);
            }
        }
        catch(Exception e1) {
            System.out.println("데이터베이스 연결 실패!"+e1.getMessage());
        }
        finally {
         try {pstmt.close();} catch (Exception ignored) {System.out.println("데이터베이스 연결 실패!" + ignored.getMessage());}
            try {con.close();}catch (Exception ignored) {System.out.println("데이터베이스 연결 실패!" + ignored.getMessage());}
            select();
        }
    }
}
