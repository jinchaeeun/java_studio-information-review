package Jsc;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumn;


public class GUIJDBCMember extends JFrame{
	JFrame frame;
	public GUIJDBCMember(String str){
		super();
		
		init();
		start();
		
	}
	
	public void init(){
	}
	
	public void start(){
        frame = new JFrame("후기"); //창 이름
        Container contentPane = frame.getContentPane();//contentPane:창 안에 필요한 부분 출력
        
        String colNames[] = { "고객명", "후기", "별점", "원룸 이름" }; //표 제목
        DefaultTableModel model = new DefaultTableModel(colNames, 0);
        JTable table = new JTable(model);  
        
        int widths[] = {30, 200,30, 30};//상대적 칸 조절 
        for (int i=0; i<4; i++) {  //위에 1개라 i는 1보다 작음 i는 0부터라는 것 명시!
         TableColumn column = table.getColumnModel().getColumn(i);
         column.setPreferredWidth(widths[i]);
         
        }
        
        contentPane.add(new JScrollPane(table), BorderLayout.CENTER); 
        String[] starList = {"★★★★★","★★★★☆","★★★☆☆","★★☆☆☆","★☆☆☆☆"};
        String[] HouseList = {"규태 원룸","도경 원룸","두리 원룸","연아 원룸","진성 원룸","채은 원룸","호시 원룸"};
        JTextField text1 = new JTextField(5);
        JTextArea text2 = new JTextArea(5,40);
        text2.setLineWrap(true);
        JComboBox<String> text3 = new JComboBox<String>(starList);//별점콤보박스로바꿔보자
        JComboBox<String> text4 = new JComboBox<String>(HouseList);
        JButton button1 = new JButton("조회");
        JButton button2 = new JButton("추가");
        JButton button3 = new JButton("삭제");
        JButton button4 = new JButton("수정");
        
        JPanel panel1 = new JPanel();
        panel1.add(new JLabel("이름:"));
        panel1.add(text1);
        /*panel1.add(new JLabel("후기:"));
        panel1.add(text2);
        panel1.add(new JLabel("별점주기"));
        panel1.add(text3);
        panel1.add(new JLabel("원룸 이름 : "));
        panel1.add(text4);*/
       
        JPanel panel2 = new JPanel();
        panel2.add(button1);
        panel2.add(button2);
        panel2.add(button3);
        panel2.add(button4);
       
        JPanel panel3 = new JPanel();
        panel3.add(new JLabel("원룸 이름 : "));
        panel3.add(text4);
        panel3.add(new JLabel("별점주기"));
        panel3.add(text3);
        
        JPanel panel4 = new JPanel();
        panel1.add(new JLabel("후기:"));
        panel1.add(text2);
        JPanel panel5 = new JPanel();
        
        
        JPanel panel = new JPanel();
        panel.setLayout(new BorderLayout());
        panel.add(panel1, BorderLayout.NORTH);//이름
        panel.add(panel4, BorderLayout.WEST);//후기
        panel.add(panel3, BorderLayout.CENTER);//원룸이름, 별점주기
        panel.add(panel2, BorderLayout.SOUTH); //버튼
        
        
        contentPane.add(panel, BorderLayout.NORTH);
     
        
        button1.addActionListener(new EventActionListener(table, text1 ,text2, text3, text4));
        button2.addActionListener(new EventActionListener(table, text1 ,text2, text3, text4));
        //button2.addItemListener(new EventItemListener(text3));
        button3.addActionListener(new EventActionListener(table));
        button4.addActionListener(new EventActionListener(table, text1, text2, text3, text4));
        //button4.addItemListener(new EventItemListener(text3));
        
        
        text1.setBounds(30, 60, 60, 20);
        
        frame.pack();
        frame.setVisible(true);
	}

}


