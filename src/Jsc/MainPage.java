package Jsc;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;



public class MainPage extends JFrame implements ActionListener{
	Container contentPane;
	ImageIcon logo, banner, dokyeong, duri, hosi, gyu, jsc;
	Font c, kyeong, dolor;
	JButton b1, b2, b3, b4;

	public MainPage() {
		b1 = new JButton ("전체 원룸 소개");
		b2 = new JButton ("후기");
		b3 = new JButton ("예약");
		
		b1.setBorderPainted(false);
		b2.setBorderPainted(false);
		b3.setBorderPainted(false);

		b1.setFocusPainted(false);
		b2.setFocusPainted(false);
		b3.setFocusPainted(false);

		b1.setBackground(new Color(255, 255, 255));
		b2.setBackground(new Color(255, 255, 255));
		b3.setBackground(new Color(255, 255, 255));
		
		add(b1);
		add(b2);
		add(b3);

		b1.setBounds(70, 40, 240, 50);
		b2.setBounds(310, 40, 240, 50);
		b3.setBounds(550, 40, 240, 50);
		
		setTitle("JSC Project");
		logo = new ImageIcon("로고.jpg");
		this.setIconImage(logo.getImage());
		banner = new ImageIcon("메인.gif");

		dokyeong = new ImageIcon("도경 원룸.jpg");
		duri = new ImageIcon("두리 원룸.jpg");
		hosi = new ImageIcon("호시 원룸.jpg");
		gyu = new ImageIcon("규태 원룸.png");
		jsc = new ImageIcon("진성 원룸.png");
		contentPane = getContentPane();
		
		MyPanel panel = new MyPanel();
		
		
		contentPane.add(panel, BorderLayout.CENTER);
		setSize(900, 1020);
		setVisible(true);

		c = new Font("고딕", Font.BOLD, 15);
		kyeong = new Font("고딕", Font.PLAIN, 13);
		dolor = new Font("고딕", Font.BOLD, 13);
		b1.addActionListener(this);
		b2.addActionListener(this);
		b3.addActionListener(this);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}
		
		@Override
		public void actionPerformed(ActionEvent e) {
			JFrame frame = new JFrame("MyPanel");
			frame.getContentPane().add(new MyPanel());
			frame.pack();
			frame.setVisible(false);
			if(e.getSource() == b2 ){
				new GUIJDBCMember("d");
				setBackground(Color.WHITE);
			}
			else if(e.getSource() == b1 ){
				new All("d");
			}
			else if(e.getSource() == b3 ){
				new ReserveGUIJDBCMember("d");
			}
			
			
}
	
	class MyPanel extends JPanel {
		public void paintComponent(Graphics g) {
			super.paintComponent(g);
			g.drawLine(50, 39, 810, 39);
			g.drawLine(50, 91, 810, 91);
			g.drawImage(logo.getImage(), 65, 150, 100, 100, this);
			g.drawImage(banner.getImage(), 215, 150, 600, 400, this);
			g.drawImage(dokyeong.getImage(), 215, 605, 180, 100, this);
			g.drawImage(duri.getImage(), 425, 605, 180, 100, this);
			g.drawImage(hosi.getImage(), 635, 605, 180, 100, this);
			g.setFont(c);
			g.drawString("NEW", 215, 590);
			g.setFont(kyeong);
			g.drawString("[JSC]도경 원룸", 215, 720);
			g.setFont(dolor);
			g.drawString("220만원", 215, 750);
			g.setFont(kyeong);
			g.drawString("[JSC]두리 원룸", 425, 720);
			g.setFont(dolor);
			g.drawString("230만원", 425, 750);
			g.setFont(kyeong);
			g.drawString("[JSC]호시 원룸", 635, 720);
			g.setFont(dolor);
			g.drawString("180만원", 635, 750);

			g.setFont(c);
			g.drawString("BEST", 215, 800);
			g.drawImage(gyu.getImage(), 215, 810, 180, 100, this);
			g.drawImage(duri.getImage(), 425, 810, 180, 100, this);
			g.drawImage(jsc.getImage(), 635, 810, 180, 100, this);
			g.setFont(kyeong);
			g.drawString("[JSC]규태 원룸", 215, 930);
			g.setFont(dolor);
			g.drawString("250만원", 215, 950);
			g.setFont(kyeong);
			g.drawString("[JSC]두리 원룸", 425, 930);
			g.setFont(dolor);
			g.drawString("230만원", 425, 950);
			g.setFont(kyeong);
			g.drawString("[JSC]진성 원룸", 635, 930);
			g.setFont(dolor);
			g.drawString("210만원", 635, 950);

			setBackground(Color.WHITE);
		}
	}


	public static void main(String[] args) {
		JFrame frame = new JFrame("MyPanel");
		new MainPage();
	}

	
	
}