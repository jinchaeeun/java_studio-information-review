package Jsc;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashSet;
import java.util.Set;

import javax.swing.JComboBox;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;


public class ReserveEventActionListener implements ActionListener {
    JTable table;
    JTextField text1, text2;
    JComboBox<String>  text3,text4;
    ReserveEventActionListener() {
    }
    ReserveEventActionListener(JTable table) {
  this.table = table;
 }
    ReserveEventActionListener(JTable table, JTextField text1, 
                      JTextField text2, JComboBox<String> text3,JComboBox<String> text4) {
        this.table = table;
        this.text1 = text1;
        this.text2 = text2;
        this.text3 = text3;
        this.text4 = text4;
    }
    
    public void actionPerformed(ActionEvent e) {
     if (e.getActionCommand().equals("조회"))
     {
      select();
     }
     else if (e.getActionCommand().equals("추가"))
     {
      //insert();
    	 boolean a = getIdByCheck((String)text3.getSelectedItem(),(String)text4.getSelectedItem());
    	 //System.out.println((String)text3.getSelectedItem()+" "+(String)text4.getSelectedItem());
    	
    	 if (a ==true )
    		 insert();
    	else
    		return;
    	 
     }
     else  if (e.getActionCommand().equals("취소"))
     {
      delete();
     }
     else
     {
    	 			//update();가 있었는데 이게 수정할때 필요한 거 라서 삭제했어요!
     }
    }
    
    public boolean getIdByCheck(String text3,String text4) {
    	System.out.println(text3+" "+text4);
        boolean result = true;
        String driver = "oracle.jdbc.driver.OracleDriver";
        String url = "jdbc:oracle:thin:@127.0.0.1:1521:xe";
        Connection con=null;
        PreparedStatement ps;
        ResultSet rs;
 
        
        try {
            
            Class.forName(driver);
            con = DriverManager.getConnection(url, "scott", "tiger");
            
            ps = con.prepareStatement("SELECT * FROM MEMBER WHERE phone=?  and tim=?");
            ps.setString(1, text3.trim());
            ps.setString(2, text4.trim());
            rs = ps.executeQuery();//실행
            if (rs.next())
                result = false; //레코드가 존재하면 false
        }catch (ClassNotFoundException e) {
        	e.printStackTrace();
        } catch (SQLException e) {
            System.out.println(e + "=>  getIdByCheck fail");
        } finally {
           
        }
 
        return result;
 
    }
    
    void select()	//조회할 때 사용하는 함수에요!
    {
        String driver = "oracle.jdbc.driver.OracleDriver";
        String url = "jdbc:oracle:thin:@127.0.0.1:1521:xe";
        Connection con = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        
        String arr[] = new String[4];
        
        DefaultTableModel model = (DefaultTableModel) table.getModel();
        int rowNum = model.getRowCount();
        for (int i=0; i<rowNum; i++)
         model.removeRow(0);
        
        try {
         String sql = "select * from member order by irum";
         
            Class.forName(driver);
            con = DriverManager.getConnection(url, "scott", "tiger");

            pstmt = con.prepareStatement(sql);
            rs = pstmt.executeQuery(sql);

            while(rs.next()) {
                arr[0] = rs.getString("irum");;				 // irum, age, phone 이름을 바꾸면 오류
                arr[1] = Integer.toString(rs.getInt("age")); //Integer을 빼고 위에랑 똑같이 바꾸면
				arr[2] = rs.getString("phone");				 //오류나서 그냥 이름이랑 번호 자리를 바꿨어요
				arr[3] = rs.getString("tim");				 // 예약시간 ?
                model.addRow(arr);
            }
        }
        catch(Exception e) {
            System.out.println(e.getMessage());
            e.printStackTrace( );
        }
        finally {
         try {rs.close();} catch (Exception ignored) {}
         try {pstmt.close();} catch (Exception ignored) {}
            try {con.close();}catch (Exception ignored) {}
        }
    }
    
    void insert()	//정보를 추가하는 함수에요!(취소나 조회 확인하는 용으로 사용했어요!)
    {
   /* 	
    	 if(  dao.getIdByCheck(id.getText()) ){ //중복아니다.(사용가능)
    		 HashSet<String> hs = new HashSet<>();
    		 set.get(JComboBox<String>());
    		 @Override
    		 public int hashCode() {
    		  
    		         return text3.hashCode();
    		 }
    		 
    		 @Override
    		 public boolean equals(Object obj) {
    		  
    		         if(!(obj instanceof Student)) {
    		                return false;
    		         }
    		         Student s = (Student)obj;
    		         return name.equals(s.name);
    		 }

    		 Set set = new HashSet();
    		 System.out.println(set); 
    		 messageBox(this,"예약 되었습니다."); 
    	 }
    	}else{ //중복이다.
             messageBox(this,"이미 예약된 시간입니다.");
    	}
    	
*/
    
             
        String arr[] = new String[4];
        arr[0] = text1.getText().trim();
        arr[1] = text2.getText().trim();
        arr[2] = text3.getSelectedItem().toString().trim();
        arr[3] = text4.getSelectedItem().toString().trim();
        System.out.println(arr[0] + " "+arr[1]+" "+arr[2]+" "+arr[3]);
        
        DefaultTableModel model = (DefaultTableModel) table.getModel();
        
        String driver = "oracle.jdbc.driver.OracleDriver";
        String url = "jdbc:oracle:thin:@127.0.0.1:1521:xe";
        Connection con = null;
        //PreparedStatement pstmt = null;
        Statement stmt=null;
        

        String sql = null;
        int rowNum;

        try {
         //sql = "insert into member (irum, age, phone, tim) values (?, ?, ?, ?)";
        	sql="insert into member values ('"+arr[0] +"',"+arr[1] +",'"+arr[2]+"','"
                   + arr[3]+"')";
        	System.out.println("test:"+sql);
            
            Class.forName(driver);
            con = DriverManager.getConnection(url, "scott", "tiger");
//            pstmt = con.prepareStatement(sql);
//            pstmt.setString(1, arr[0]); 
//            pstmt.setInt(2, Integer.parseInt(arr[1])); 
//            pstmt.setString(3, arr[2]);
//            pstmt.setString(4, arr[3]);	
            

 //           rowNum = pstmt.executeUpdate();
            stmt=con.createStatement();
            rowNum=stmt.executeUpdate(sql);
            
            if (rowNum == 0)
             System.out.println("예약 실패!");
            else
            {
             model.addRow(arr);
             System.out.println("예약 성공!");
            }
        }
        catch(Exception e1) {
            System.out.println("예약 실패!! = "+e1.getMessage());
        }
        finally {
         try {stmt.close();} catch (Exception ignored) {}
            try {con.close();}catch (Exception ignored) {}
        }
    }
    
    void delete()	//취소하는 함수에요!
    {
    	
    		/*JOptionPane.showMessageDialog(null, "취소 되었습니다!");*/
    	   int result = JOptionPane.showConfirmDialog(null, "정말 취소하시겠습니까?", "취소", 
       			JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
       	
           /*if(result == JOptionPane.CLOSED_OPTION) {
        	  
           }*/
           if(result==JOptionPane.YES_OPTION){
        	   int row = table.getSelectedRow();
               if (row == -1)
                   return;
           else {
          			
           	   }
               DefaultTableModel model = (DefaultTableModel) table.getModel();
               String irum = model.getValueAt(row, 0).toString().trim();
              
               String driver = "oracle.jdbc.driver.OracleDriver";
               String url = "jdbc:oracle:thin:@127.0.0.1:1521:xe";
               Connection con = null;
               PreparedStatement pstmt = null;

               String sql = null;
               int rowNum;		//rowNum??? result 1 ,2 이런식으로 숫자 넘어가게 하는???
               
               try {  
                   sql = "delete from member where irum = ?";

                   Class.forName(driver);
                   con = DriverManager.getConnection(url, "scott", "tiger");
                   pstmt = con.prepareStatement(sql);
                   pstmt.setString(1, irum);
                   rowNum = pstmt.executeUpdate();
                   	 
                   
                   if (rowNum == 0)
                    System.out.println("예약 취소 실패!");
                   else
                   {
                    model.removeRow(row); 
                    System.out.println("예약 취소 성공!");
                   }										
               }
               catch(Exception e1) {
                   System.out.println("예약 취소 실패!"+e1.getMessage());
               }
               finally {
            	   
                try {pstmt.close();} catch (Exception ignored) {}
                   try {con.close();}catch (Exception ignored) {}
                   }
               }
           }
     
       	// 버튼은 나오는데 예를 눌러도 취소가 되고 아니오를 눌러도 취소가되는 오류가나요... 
    	// 위치 수정하니까 되는것 같아여! 
    	
     /*int row = table.getSelectedRow();
        if (row == -1)
            return;
        
        DefaultTableModel model = (DefaultTableModel) table.getModel();
        String irum = model.getValueAt(row, 0).toString().trim();
       
        String driver = "oracle.jdbc.driver.OracleDriver";
        String url = "jdbc:oracle:thin:@127.0.0.1:1521:xe";
        Connection con = null;
        PreparedStatement pstmt = null;

        String sql = null;
        int rowNum;		//rowNum??? result 1 ,2 이런식으로 숫자 넘어가게 하는???
        
        try {  
            sql = "delete from member where irum = ?";

            Class.forName(driver);
            con = DriverManager.getConnection(url, "scott", "tiger");
            pstmt = con.prepareStatement(sql);
            pstmt.setString(1, irum);
            rowNum = pstmt.executeUpdate();
            	 
            
            if (rowNum == 0)
             System.out.println("예약 취소 실패!");
            else
            {
             model.removeRow(row); 
             System.out.println("예약 취소 성공!");
            }										
        }
        catch(Exception e1) {
            System.out.println("예약 취소 실패!"+e1.getMessage());
        }
        finally {
         try {pstmt.close();} catch (Exception ignored) {}
            try {con.close();}catch (Exception ignored) {}
            }
        }
*/


    
    /*void update()
    {
     int i, age, rowNum;
     String irum = null, phone = null, table_irum = null;
        DefaultTableModel model = (DefaultTableModel) table.getModel();
        
        irum = text1.getText().toString().trim();
        age = Integer.parseInt(text2.getText().toString().trim());
        phone = text3.getText().toString().trim();
        
        String driver = "oracle.jdbc.driver.OracleDriver";
        String url = "jdbc:oracle:thin:@127.0.0.1:1521:xe";
        Connection con = null;
        PreparedStatement pstmt = null;

        String sql = null;
        
        try {  
            sql = "update member set age = ?, phone = ? where irum = ?";
            System.out.println("sql : " + sql);
            Class.forName(driver);
            con = DriverManager.getConnection(url, "scott", "tiger");
            pstmt = con.prepareStatement(sql);
            pstmt.setInt(1, age); 
            pstmt.setString(2, phone);
            pstmt.setString(3, irum); 
            rowNum = pstmt.executeUpdate();
            
            if (rowNum == 0)
             System.out.println("데이터베이스 수정 실패!");
            else
            {
             rowNum = model.getRowCount();
                for (i=0; i<rowNum; i++)
                {
                 table_irum = model.getValueAt(i, 0).toString().trim();
                 if (table_irum.equals(irum))
                 {
                  model.setValueAt(irum, i, 0);
                  model.setValueAt(age, i, 1);
                  model.setValueAt(phone, i, 2);
                  break;
                 }
                }
             System.out.println("데이터베이스 수정 성공!=" + rowNum);
            }
        }
        catch(Exception e1) {
            System.out.println("데이터베이스 연결 실패!"+e1.getMessage());
        }
        finally {
         try {pstmt.close();} catch (Exception ignored) {System.out.println("데이터베이스 연결 실패!" + ignored.getMessage());}
            try {con.close();}catch (Exception ignored) {System.out.println("데이터베이스 연결 실패!" + ignored.getMessage());}
            select();
        }
    }
}*/
}

