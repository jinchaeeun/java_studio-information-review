package Jsc;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumn;
public class ReserveGUIJDBCMember extends JFrame{
	JFrame frame;
	public ReserveGUIJDBCMember(String str){
		super();
		
		init();
		si();
		
	}
	
	public void init(){
	}
	
	public void si(){
        JFrame frame = new JFrame("예약 프로그램");
        frame.setPreferredSize(new Dimension(600, 250));
        frame.setLocation(360, 400);
        Container contentPane = frame.getContentPane();
        
        String colNames[] = {"예약자 이름","번호","예약시간", "원룸이름" };	// Integer함수가 변경하면 오류가 나서 이름이랑 번호 자리를 바꿨어요!
        DefaultTableModel model = new DefaultTableModel(colNames, 0);
        JTable table = new JTable(model);  
        
        int widths[] = {90, 90, 40, 170};
        for (int i=0; i<4; i++) {	//i의 수는 위에 이름,번호,예약시간 객수에요!
         TableColumn column = table.getColumnModel().getColumn(i);
         column.setPreferredWidth(widths[i]);
        }
        
        contentPane.add(new JScrollPane(table), BorderLayout.CENTER);
        String[] s = {"규태 원룸","도경 원룸","두리 원룸","연아 원룸","진성 원룸","채은 원룸","호시 원룸"};
        String[] n = {"10시~11시","11시~12시","12시~13시","13시~14시","14시~15시","15시~16시","16시~17시"};

        
        JTextField text1 = new JTextField(4);
        JTextField text2 = new JTextField(3);
        JComboBox<String> text3 = new JComboBox<String>(n);
        JComboBox<String> text4 = new JComboBox<String>(s);
        JButton button1 = new JButton("조회");
        JButton button2 = new JButton("추가");
        JButton button3 = new JButton("취소");
     /*   JButton button4 = new JButton("수정");*/
        
        JPanel panel1 = new JPanel();
        panel1.add(new JLabel("예약자 이름"));
        panel1.add(text1);
        panel1.add(new JLabel("번호"));
        panel1.add(text2);
        panel1.add(new JLabel("예약 시간"));
        panel1.add(text3);
        panel1.add(new JLabel("원룸 이름"));
        panel1.add(text4);
       
        JPanel panel2 = new JPanel();
        panel2.add(button1);
        panel2.add(button2);
        panel2.add(button3);
        /*panel2.add(button4);*/
        
        JPanel panel = new JPanel();
        panel.setLayout(new BorderLayout());
        panel.add(panel1, BorderLayout.CENTER);
        panel.add(panel2, BorderLayout.SOUTH);
        contentPane.add(panel, BorderLayout.SOUTH);
        
        button1.addActionListener(new ReserveEventActionListener(table, text1, text2, text3,text4));
        button2.addActionListener(new ReserveEventActionListener(table, text1, text2, text3,text4));
        button3.addActionListener(new ReserveEventActionListener(table, text1, text2, text3,text4));
        /*button4.addActionListener(new EventActionListener(table, text1, text2, text3));*/

        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }
}


