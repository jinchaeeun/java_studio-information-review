package Jsc;

import java.awt.BorderLayout;
import java.awt.Button;
import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.Frame;
import java.awt.GridLayout;
import java.awt.Panel;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Calendar;
import java.util.GregorianCalendar;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.LineBorder;


class Calender extends JPanel implements ActionListener 

{
       String [] days = {"일","월","화","수","목","금","토"};
       int year,month,day,todays,memoday=0;
       Font f;  //글꼴
       Color bc,fc; //토,일 색깔
       Calendar today;
       Calendar cal;
       JButton btnBefore,btnAfter;
       JButton[] calBtn = new JButton[49];
       JLabel thing;
       JLabel time;
       JPanel panWest;
       JPanel panNorth;
       JLabel txtMonth; 
       JLabel txtYear;
       JTextField txtTime;
       BorderLayout bLayout= new BorderLayout();     
       ////////////////////////////////////////
       public Calender(){
             today = Calendar.getInstance(); //디폴트의 타임 존 및 로케일을 사용해 달력을 가져옵니다.
             cal = new GregorianCalendar();
             year = today.get(Calendar.YEAR);
             month = today.get(Calendar.MONTH)+1;//1월의 값이 0 
             
             panNorth = new JPanel();
             panNorth.setBackground(Color.white);
			 panNorth.add(btnBefore = new JButton("◀")); 
			 panNorth.add(txtYear = new JLabel(year+"년"));
             panNorth.add(txtMonth = new JLabel( month+"월"));
             txtYear.setEnabled(true);
			 txtMonth.setEnabled(true);
             panNorth.add(btnAfter = new JButton("▶"));
             f=new Font("Sherif",Font.BOLD,18);
             txtYear.setFont(f);
             txtMonth.setFont(f);       
             
             btnBefore.setBackground(Color.white);
             btnBefore.setPreferredSize(new Dimension(60,30));
             btnBefore.setBorder(new LineBorder(Color.white));
             
             btnAfter.setBackground(Color.white);
             btnAfter.setPreferredSize(new Dimension(60,30));
             btnAfter.setBorder(new LineBorder(Color.white));
             
             
             add(panNorth,"North");
                   
                         
             // 달력의 날에 해당하는 부분
             panWest = new JPanel(new GridLayout(7,7));//격자나,눈금형태의 배치관리자
             panWest.setBackground(new Color(247,202,201));
             f=new Font("Sherif",Font.BOLD,12);
             gridInit();
             calSet();
             hideInit();
             add(panWest,"Center");
             
                         
             btnBefore.addActionListener(this);
             btnAfter.addActionListener(this); 
             
             setVisible(true);
             setBackground(Color.white);
           //  setBounds(0,0,360,450);
              }	
        
       

       public void calSet(){
             cal.set(Calendar.YEAR,year);
             cal.set(Calendar.MONTH,(month-1));
             cal.set(Calendar.DATE,1);
             int dayOfWeek = cal.get(Calendar.DAY_OF_WEEK);
             
             /*
              * get 및 set 를 위한 필드치로, 요일을 나타냅니다.
              * 이 필드의 값은,SUNDAY,MONDAY,TUESDAY,WEDNESDAY
              * ,THURSDAY,FRIDAY, 및 SATURDAY 가 됩니다. 
              * get()메소드의 의해 요일이 숫자로 반환
              */
             int j=0;
             int hopping=0;
             hopping=j;
             calBtn[0].setForeground(new Color(255,0,0));//일요일 "일"
             calBtn[6].setForeground(new Color(0,0,255));//토요일 "토"
             for(int i=cal.getFirstDayOfWeek();i<dayOfWeek;i++){
            	
            	calBtn[j+7].setBackground(Color.white);
              	calBtn[j+7].setBorder(new LineBorder(Color.pink));
              	
             	j++;
             	
             }
             /*
              * 일요일부터 그달의 첫시작 요일까지 빈칸으로 셋팅하기 위해 
              */
                          
             hopping=j;
             for(int kk=0;kk<hopping;kk++){
                    calBtn[kk+7].setText("");
             }
             for(int i=cal.getMinimum(Calendar.DAY_OF_MONTH);
                           i<=cal.getMaximum(Calendar.DAY_OF_MONTH);i++){
                 cal.set(Calendar.DATE,i);
                    if(cal.get(Calendar.MONTH) !=month-1){
                           break;
                    }
              
                    todays=i;
		            if(memoday==1){
                           calBtn[i+6+hopping].setForeground(new Color(0,255,0));  
                    }
                    else{
                           calBtn[i+6+hopping].setForeground(new Color(0,0,0));
                           calBtn[i+6+hopping].setBackground(Color.white);
                           calBtn[i+6+hopping].setBorder(new LineBorder(Color.pink));
                           if((i+hopping-1)%7==0){//일요일
                                 calBtn[i+6+hopping].setForeground(new Color(255,0,0));
                           }
                           if((i+hopping)%7==0){//토요일
                                 calBtn[i+6+hopping].setForeground(new Color(0,0,255));
                           }
                    }
                    /*
                     * 요일을 찍은 다음부터 계산해야 하니 요일을 찍은 버튼의 갯수를 더하고
                     * 인덱스가 0부터 시작이니 -1을 해준 값으로 연산을 해주고
                     * 버튼의 색깔을 변경해준다. 
                     */
                    calBtn[i+6+hopping].setText((i)+"");
                    
             }//for
       }//end Calset()
       public void actionPerformed(ActionEvent ae){         
            if(ae.getSource() == btnBefore){
                    this.panWest.removeAll();
                    calInput(-1);
                    gridInit();
                    panelInit();               
                    calSet();
                    hideInit();
                    this.txtYear.setText(year+"년");
                    this.txtMonth.setText(month+"월");                   
             }                   
             else if(ae.getSource() == btnAfter){
                    this.panWest.removeAll();
                    calInput(1);
                    gridInit();
                    panelInit();
                    calSet();
                    hideInit();
                    this.txtYear.setText(year+"년");
                    this.txtMonth.setText(month+"월");                                       
             }
         /*    else if(Integer.parseInt(ae.getActionCommand()) >= 1 && 
                      Integer.parseInt(ae.getActionCommand()) <=31){
                      day = Integer.parseInt(ae.getActionCommand());
                      //버튼의 밸류 즉 1,2,3.... 문자를 정수형으로 변환하여 클릭한 날짜를 바꿔준다.
                      System.out.println(+year+"-"+month+"-"+day);
                      calSet();
             	}*/
             }      
      //end actionperformed()
       public void hideInit(){
            for(int i = 0 ; i < calBtn.length;i++){
                    if((calBtn[i].getText()).equals(""))
                           calBtn[i].setEnabled(false);
                    //일이 찍히지 않은 나머지 버튼을 비활성화 시킨다. 
             }//end for
       }//end hideInit()

       public void gridInit(){
         //jPanel3에 버튼 붙이기
         for(int i = 0 ; i < days.length;i++)
		   {
              panWest.add(calBtn[i] = new JButton(days[i]));
			  calBtn[i].setContentAreaFilled(false);
			  calBtn[i].setBorderPainted(false);
		   }	
			  for(int i = days.length ; i < 49;i++){                
                    panWest.add(calBtn[i] = new JButton(""));                   
                    calBtn[i].addActionListener(this);
             }              
       }//end gridInit()
	   public void panelInit(){
         GridLayout gridLayout1 = new GridLayout(7,7);
         panWest.setLayout(gridLayout1);   
       }//end panelInit()
       public void calInput(int gap){
             month+=(gap);
             if (month<=0){
                           month = 12;
                           year  =year- 1;
             }else if (month>=13){
                           month = 1;
                           year =year+ 1;
             }
       }//end calInput()
}//end class

/*
class Reserve2 extends JPanel{
	Button bn;
	
	public Reserve2() {
		
		bn=new Button("버튼");
		
		super.add(bn);
		setBackground(Color.WHITE);
		setSize(200,200);
		setVisible(true);
	}
}
*/

class Reserve extends JFrame{
	Calender ca = new Calender();
//	Reserve2 re = new Reserve2();
	
	/*
	super.setLayout(null);	
    ca.setBounds(0,0,360,700);
    super.add(ca);
 }*/
	public Reserve() {
		
		super.setTitle("예약프로그램");		
		super.setLayout(null);		
		ca.setBounds(0,0,360,300);
//		re.setBounds(390,0,200,200);
	    super.add(ca);		
//	    super.add(re);
		
		super.setSize(900,700);		
		super.setBackground(Color.white);
		super.setLocationRelativeTo(null);
		super.setVisible(true);
	}
}


public class project_test2 {
	public static void main(String[] args) {
		 new Reserve();
         //new Calender();
         
                  
	}
}



